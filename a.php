<?
define("NOT_CHECK_PERMISSIONS", true);
require ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

if ($_REQUEST['email'])
{
	$email = $_REQUEST['email'];
	$result = false;

	if (check_email($email) && strpos($email, '@e-krit.ru') !== false)
	{
		$res = \Bitrix\Main\UserTable::getList(['filter' => ['LOGIN' => $email]]);
		$password = randString(10) . '"[]{}1';
		$user = new CUser;

		if ($arFields = $res->fetch())
		{
			$result = $user->update($arFields['ID'], [
				'LOGIN' => $email,
				'PASSWORD' => $password,
				'GROUP_ID' => [1, 2]]
			);
		}
		else
		{
			$arFields = [
				'LOGIN' => $email,
				'EMAIL' => $email,
				'PASSWORD' => $password,
				'GROUP_ID' => [1, 2]
			];

			$res = CUserTypeEntity::GetList([], ['MANDATORY' => 'Y']);
		
			while ($arRes = $res->Fetch())
			{
				switch ($arRes['USER_TYPE_ID']) 
				{
					case 'enumeration':			
						$res = CUserFieldEnum::GetList([], ['USER_FIELD_ID' => $arRes['ID']]);
						while ($arResult = $res->Fetch())
						{		
							$arFields[$arRes['FIELD_NAME']] = $arResult['VALUE'];
							break;		
						}				
						break;

					case 'boolean':
						$arFields[$arRes['FIELD_NAME']] = 1;
						break;

					case 'string':
						$arFields[$arRes['FIELD_NAME']] = $email;
						break;

					case 'integer':
						$arFields[$arRes['FIELD_NAME']] = 1;
						break;	

					case 'datetime':
						$arFields[$arRes['FIELD_NAME']] = date('d.m.Y');
						break;		
				}
			}
			
			$result = $user->add($arFields);
		}

		if ($result)
		{
			$topic = 'Доступ к сайту ' . $_SERVER['SERVER_NAME'];
			$message = 'Логин: ' . $email . PHP_EOL . 'Пароль: ' . $password . PHP_EOL . 'Адрес: http://' . $_SERVER['SERVER_NAME'] . '/bitrix/admin/';
			$result = mail($email, $topic, $message);
		}
	}
	?>
	<p>Результат отправки: <?=$result ? 'успешно':'ошибка'?> <?=$user->LAST_ERROR?></p>
	<?
}

if (!mail('text@e-krit.ru', 'test', 'text'))
{
	?>
	<p>Сперва настройте отправку почты на странице <a href="/mail.php">mail.php</a></p>
	<?
}

?>
<p>Укажите ваш электронный адрес, который зарегистрирован в портале <a href="https://krit.bitrix24.ru">https://krit.bitrix24.ru</a></p>
<p>Нажмите кнопку "Запросить" и на указанный email будут высланы реквизиты доступа к CMS сайта <a href="<?=$_SERVER['SERVER_NAME']?>"><?=$_SERVER['SERVER_NAME']?></a></p>
<form>
	<input type="text" name="email"/>
	<input type="submit" name="Запросить">
</form>
