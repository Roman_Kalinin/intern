<?
require ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
require ('/server-tools/src/site-manager/vendor/autoload.php');

CEvent::checkEvents();

if ($_REQUEST['setupMail'] == 'Y')
{
	$site = new Krit\Site\Site($_SERVER['SERVER_NAME']);
	Krit\Policy\Manager::executeWithLowPrivilege($site, 'setupMail');
}

if ($_REQUEST['setupDevTools'] == 'Y' && !defined('DEV_ENV'))
{
	$dbConnPath = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/dbconn.php';
	$dbConn = file_get_contents($dbConnPath);
	$dbConn = str_replace('?>', "@include('/server-tools/src/dev-bootstrap.php');?>", $dbConn);
	file_put_contents($dbConnPath, $dbConn);
	?>
	<script type="text/javascript">
		window.location.reload();
	</script>
	<?
}

$mailStatus = mail('noreply@e-krit.ru', 'mail status', 'mail status');
$devMailServiceStatus = function_exists('custom_mail') && defined('DEV_SETTING_NAME');
$bAgenOnCron = COption::GetOptionString("main", "agents_use_crontab", "N") == 'N' &&
	COption::GetOptionString("main", "check_agents", "Y") == 'N'; 

if ($_REQUEST['email'])
{
	COption::setOptionString('main', DEV_SETTING_NAME, $_REQUEST['email']);
}

$output = shell_exec('crontab -l');
$cronEventsFileName = false;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/cron_events.php'))
{
	$cronEventsFileName = $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/cron_events.php';
}
elseif (file_exists($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/cron_events.php'))
{
	$cronEventsFileName = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/cron_events.php';
}

if ($_REQUEST['runCronEvents'] == 'y')
{
	$lastMailBulk = COption::GetOptionString("main", "mail_event_bulk");
	COption::SetOptionString("main", "mail_event_bulk", "100");
	system('php ' . $cronEventsFileName . ' > /dev/null 2>&1');
	COption::SetOptionString("main", "mail_event_bulk", $lastMailBulk);
}

if ($_REQUEST['clearQueue'] == 'y')
{
	$DB->Query('DELETE From b_event');
}

$bCronStatus = false;

if ($cronEventsFileName && defined('DEV_ENV'))
{
	$cronCommand = '* * * * * php ' . $cronEventsFileName . ' > /dev/null 2>&1';
	
	$bCronStatus = (strpos($output, $cronCommand) !== false);

	if (!$bCronStatus && $_REQUEST['setupCronEvents'] == 'Y')
	{
		shell_exec('crontab -l > crontab.cfg');
		shell_exec('echo "SHELL=/bin/bash" >> crontab.cfg');
		shell_exec('echo "PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin" >> crontab.cfg');
		shell_exec('echo "MAILTO=\"\"" >> crontab.cfg');
		shell_exec('echo "' . $cronCommand . '" >> crontab.cfg');
		shell_exec('crontab crontab.cfg');
		unlink('crontab.cfg');
		?>
		<script type="text/javascript">
			window.location.reload();
		</script>
		<?
	}
}

$currentEmail = COption::getOptionString('main', DEV_SETTING_NAME);
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
<style type="text/css">
html{
	font-family: "Open Sans";
}
#magnifying{
    position: absolute;
    right: -29px;
    top: 6px;
}
a{
	font-family: "Open Sans";
	display: inline-block;
	vertical-align: top;
	font-size: 14px;
	color: #404040;
	position: relative;
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	-o-transition: all 0.3s;
	transition: all 0.3s;
}
a:hover{
	text-decoration: none;
	color: #419b00;
}

p{
	font-family: "Open Sans";
}
.main-block{
	width: 900px;
	margin: 0 auto;
	position: relative;
}
h1{
	text-align: center;
	text-transform: uppercase;
	font-family: "Open Sans";
    margin-bottom: 24px;
    margin-top: 35px;
    line-height: 33px;
}
h1 a{
	display: block;
	font-size: 24px;
	text-transform: none;
	color: #43e700;
	margin-top: 10px;
	text-decoration: none;
}

.success a{
	color: #fff;
	font-size: 16px;
	font-weight: 500;
}
.success{
	position: relative;
}
a.success:after,
.hover-block:after,
.bitrix:after{
	content: '';
	left: 0;
	position: absolute;
	top: 0;
	display: block;
	opacity: 0;
	padding: 9px 14px 7px 11px;
	top: 23px;
}
a.success:hover:after{
	content: 'Данная ветка используется на сайте';
    position: absolute;
    z-index: 20;
    white-space: nowrap;
    background-color: #e0e5e9;
    color: #404040;
    opacity: 1;
    font-size: 14px;
}
.hover-block:hover:after,
.bitrix:hover:after{
	content: 'Нажать для смены ветки';
	padding: 9px 14px 7px 11px;
    position: absolute;
    top: 23px;
    z-index: 20;
    white-space: nowrap;
    background-color: #e0e5e9;
    color: #404040;
    opacity: 1;
}
.bitrix:hover:after{
	content: 'Просмотреть задачу в bitrix24';
}
a.success:before,
.hover-block:before,
.bitrix:before{
	content: '';
    position: absolute;
    top: 19px;
    z-index: 21;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 0 4px 4px 4px;
    border-color: transparent transparent #e0e5e9 transparent;
    opacity: 0;
}
a.success:hover:before,
.hover-block:hover:before,
.bitrix:hover:before{
	opacity: 1;
	left: 22px;
}
.hover-block:hover:before{
	left: 7px;
}
.bitrix{
	display: block;
}
.bitrix:hover:before{
	left: 0;
    right: 0;
    margin-left: auto;
    margin-right: auto;
}
.title-info{
	display: inline-block;
	vertical-align: top;
	width: 50%;
	float: left;
	font-size: 18px;
    font-family: "Open Sans";
    font-weight: 900;
    padding: 10px;
    text-align: center;
}
.block-info{
	display: inline-block;
	vertical-align: top;
	float: left;
	width: 50%;
	min-height: 40px;
	padding: 10px;
	border-left: 1px solid #bdbdbd;
}
.block-info.center{
	text-align: center;
}
.block-info.center .bitrix:hover:after{
	right: 0;
    left: 0;
}
.main-block-info{
	width: 100%;
	float: left;
	border-bottom: 1px solid #edf3f7;
	border-right: 1px solid #edf3f7;
	max-width: 669px;
}
.branch-task{
	width: 100%;
	float: left;
	max-height: 500px;
	position: relative;
	overflow: scroll;
	border-bottom: 1px solid #edf3f7;
}
.branch-task:after{
	top: 0;
	position: absolute;
	left: 0;
	content: '';
	width: 669px;
	height: 1px;
	background-color: #edf3f7;
}
.success.main-block-info{
	background-color: #419b00;
}
*:focus{
	outline: none;
}
.hidden-block{
	display: none;
}
.search-block{
	display: block;
	width: 100%;
	float: left;
	margin-top: -37px;
	position: relative;
}
label{
    font-family: "Open Sans";
    display: inline-block;
    vertical-align: top;
    font-size: 14px;
    font-weight: normal;
    color: #404040;
    position: relative;
    width: 145px;
    margin-bottom: 0;
    padding-top: 6px;
}
input[type="text"]{
	font-family: "Open Sans";
    background: none;
    border: none;
    border-bottom: 2px solid #bdbdbd;
   	display: block;
   	margin: 0 auto;
    max-width: 850px;
    width: 100%;
    padding-bottom: 10px;
    padding-left: 14px;
    padding-right: 14px;
    margin-top: 20px;
    margin-bottom: 20px;
    padding-top: 0;
    border-radius: 0;
    -webkit-transition: all 0.24s;
    -moz-transition: all 0.24s;
    -o-transition: all 0.24s;
    transition: all 0.24s;
    color: #231f20;
}
input[type="text"]:hover, 
input[type="tel"]:hover, 
input[type="tel"]:focus,
input[type="text"]:focus,
textarea:focus,
textarea:hover {
	border-color: #64dd17;
}
input[type="submit"]:hover, .send-event-btn:hover,
input[type="submit"]:focus, .send-event-btn:focus {
	border-color: #43e700;
    color: #ffffff;
}
input[type="submit"]:active, .send-event-btn:active {
	box-shadow: none;
}
input[type="submit"], .send-event-btn{
	border-style: solid;
    border-width: 2px;
    border-radius: 5px;
    color: #ffffff;
    display: block;
    vertical-align: top;
	font-family: "Open Sans";
    text-transform: uppercase;
    font-size: 16px;
    position: relative;
    -webkit-transition: all 0.24s;
    -moz-transition: all 0.24s;
    -o-transition: all 0.24s;
    transition: all 0.24s;
    border-bottom: 0;
    border-color: #43e700;
    color: #ffffff;
    padding-left: 0;
    padding-right: 0;
    padding-top: 19px;
    padding-bottom: 15px;
    width: 166px;
    text-align: center;
	border-color: rgb(135, 234, 137);
    background-image: -moz-linear-gradient( 90deg, rgb(36,198,23) 0%, rgb(63,218,0) 100%);
    background-image: -webkit-linear-gradient( 90deg, rgb(36,198,23) 0%, rgb(63,218,0) 100%);
    background-image: -ms-linear-gradient( 90deg, rgb(36,198,23) 0%, rgb(63,218,0) 100%);
    box-shadow: 0px 2px 0px 0px rgba(5, 105, 0, 0.2);
	margin: 0 auto;
}
.send-event-btn {
    display: inline-block;
    width: auto;
    padding: 10px 20px;
    margin-bottom: 30px;
}
.svg-href{
	position: absolute;
	top: 0;
	left: 0;
	-webkit-transition: all ease-out 0.4s;
	-moz-transition: all ease-out 0.4s;
	-o-transition: all ease-out 0.4s;
	transition: all ease-out 0.4s;
}
.svg-href .word-ekrit{
	-webkit-transition: all ease-out 0.4s;
	-moz-transition: all ease-out 0.4s;
	-o-transition: all ease-out 0.4s;
	transition: all ease-out 0.4s;
}
.svg-href:hover .word-ekrit{
	fill:#86ff54;
}

.border-input span {
	display: block;
	text-align: center;
    vertical-align: top;
    color: #333;
    font-size: 18px;	
}
.work {
	color: #43e700;
	font-size: 18px;
}
.not-work {
	color: rgb(255, 62, 74);
	font-size: 18px;
}
.bold {
	font-weight: bold;
	font-size: 16px;
}
.status {
	font-size: 18px;
}
.status-block {
	line-height: 28px;
}
hr{
	border-top: 2px solid #bdbdbd;
}
h2{
	text-align: center;
	margin-bottom: 30px;
	margin-top: 30px;
}
table{
    width: 100%;
    margin-bottom: 20px;
}
th{
    border-right: 2px solid #ffffff; 
    font-size: 18px;
    color: #ffffff;
    text-align: center;
    padding: 20px 40px;
}
thead{
    background-color: #292929;
    border: 1px solid #ffffff;
}
th:last-child{
    border-right: 0;
}
tbody td:first-child{
    border-left: 1px solid #ffffff;
    white-space: nowrap;
}
tbody td{
    font-family: Arial;
    color: #616161;
    padding: 30px 40px;
    border-right: 2px solid #ffffff;
    border-bottom: 2px solid #ffffff;
}
tbody td:last-child {
	text-align: center;
}
tbody tr:nth-child(even){
    background: #ececec;
}
tbody tr:nth-child(odd){
    background: #ffffff;
}
.status-block__setup{
	color: #e7ac00;
	text-decoration: underline;
	font-size: 18px
}
/* Start:/local/templates/krit/css/loader.css?15239638591240*/
.loader {
	border: 3px solid transparent;
	border-top: 5px solid #3fda00;
	border-bottom: 5px solid #3fda00;
	border-radius: 50%;
	width: 100px;
	height: 100px;
	-webkit-animation: spin 2s linear infinite;
	-moz-animation: spin 2s linear infinite;
	-ms-animation: spin 2s linear infinite;
	-o-animation: spin 2s linear infinite;
	animation: spin 2s linear infinite;
}
.loader-block {
	position: absolute;
	left: 0;
	right: 0;
	margin-left: auto;
	margin-right: auto;
	width: 100px;
	height: 100px;
	top: 35%;
	display: flex;
	align-items: center;
	justify-content: center;
}
#loader {
	display: none;
    width: 100%;
    height: 100%;
    position: fixed;
    left: 0;
    top: 0;
    background-color: #fff;
    z-index: 100;
}
#loader .loader-svg{
	position: absolute;
    left: 0;
    right: 0;
    margin-left: auto;
    margin-right: auto;
}
@keyframes spin {
	0% {
	-moz-transform: rotate(0deg);
	-ms-transform: rotate(0deg);
	-webkit-transform: rotate(0deg);
	-o-transform: rotate(0deg);
	transform: rotate(0deg);
	}
	100% {
	-moz-transform: rotate(360deg);
	-ms-transform: rotate(360deg);
	-webkit-transform: rotate(360deg);
	-o-transform: rotate(360deg);
	transform: rotate(360deg);
	}
}

/* End */
</style>
<div class="main-block">
<h1>Управление почтой
<a href="http://<?=$_SERVER['SERVER_NAME']?>" target="_blank"><?=$_SERVER['SERVER_NAME']?></a>
</h1>
<p>
	Панель управления почтой позволяет указать <b>единого получателя</b> для всех писем, отпровляемых с сайта. При этом, в тему письма дописываются адресаты, которые получать письмо при переносе на боевую площадку.
</p>
<p>
	Инициируйте на сайте событие отправки почты, и нажмите кнопку "Отправить все письма".
</p>
<p class="status-block">
	<span class="status">Статус работы почты:</span> <?= $mailStatus ? '<span class="work">работает</span>' : '<span class="not-work">не работает <a class="status-block__setup" href="?setupMail=Y">настроить (в течение 3-х минут)</a></span>'?><br>
	<span class="status">Статус системы dev-писем:</span> <?= $devMailServiceStatus ? '<span class="work">работает</span>' : '<span class="not-work">не работает <a class="status-block__setup" href="?setupDevTools=Y">настроить</a></span>'?><br>
	<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

	<script type="text/javascript">
		$(document).on('click', '.js-send-event-btn', function(event) {
			event.preventDefault();
			$('.js-send-event-btn').remove();
			$('#loader').show();
			$.get('?runCronEvents=y', function(response) {
				$('#loader').hide();
				$('.js-event-logs-table').replaceWith($(response).find('.js-event-logs-table'));
			});
		});

		$(document).on('click', '.js-clear-event-btn', function(event) {
			event.preventDefault();
			$('#loader').show();
			$('.js-clear-event-btn').remove();
			$.get('?clearQueue=y', function(response) {
				$('#loader').hide();
				$('.js-event-logs-table').replaceWith($(response).find('.js-event-logs-table'));
			});
		});
	</script>

	<?/*
	<span class="status">Работа агентов на cron:</span> <?= $bAgenOnCron ? '<span class="work">да</span>' : '<span class="work">нет</span>'?><br>
	<span class="bold">Внимание! Если агенты выполянются на cron, то за отправку писем отвечает настройка почты на main-хосте.</span>

	<?if ($bAgenOnCron):?>
		<span class="status">Статус работы cron-отправки: </span><?= $bCronStatus ? '<span class="work">да</span>' : '<span class="work">нет <a class="status-block__setup" href="?setupCronEvents=Y">настроить</a></span>'?><br>
	<? endif */?>
</p>
<hr>
<form>
<div class="border-input">
	<span>Email получателя для всех писем:</span>
	<div>
		<input type="text" name="email" value="<?=$currentEmail?>">
	</div>
	<input type="submit" name="Сохранить" value="Сохранить">
</div>
</form>
<hr>
<h2>Состояние очереди почовых сообщений:</h2>
<?
global $DB;
$res = $DB->Query('SELECT * From b_event ORDER BY ID DESC LIMIT 20');
?>
<a href="#" class="send-event-btn js-send-event-btn hidden">Отправить все пиcьма</a>
<? if ($res->SelectedRowsCount() > 0): ?>
	<a href="#" class="send-event-btn js-clear-event-btn">Очистить очередь</a>
<? endif ?>
<table class="table js-event-logs-table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Тип события</th>
			<th>Данные</th>
			<th>Дата создания</th>
			<th>Дата отправки</th>
			<th>Статус отправки</th>
		</tr>
	</thead>
	<tbody>
	<?php
	while ($arFields = $res->fetch()):?>
		<tr>
			<td><?=$arFields['ID']?></td>
			<td><?=$arFields['EVENT_NAME']?></td>
			<td><?=htmlentities(substr($arFields['C_FIELDS'], 0, 32))?></td>
			<td><?=$arFields['DATE_INSERT']?></td>
			<td><?=$arFields['DATE_EXEC']?></td>
			<td align="center"><?=$arFields['SUCCESS_EXEC']?></td>
			<? if ($arFields['SUCCESS_EXEC'] == 'N'): ?>
				<script type="text/javascript">
					$('.js-send-event-btn').removeClass('hidden');
				</script>
			<? endif ?>
		</tr>
	<?endwhile?>
	</tbody>
</table>
<a class="svg-href" href="https://www.e-krit.ru">
	<svg version="1.1" id="e-krit" width="136" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 264.8 80.8" enable-background="new 0 0 264.8 80.8" xml:space="preserve">
	<g>
		<path fill="#43e700" class="word-ekrit" d="M130.8,62.4l-10.3,0l0-10.9c0-2.2-1-4.1-2.9-5.6c-1.9-1.5-4.3-2.3-7.2-2.3h-6.5l0,18.7l-10.3,0l0-45.2
			l10.3,0l0,18.5l6.5,0c2.9,0,5.3-0.7,7.2-2.2c1.9-1.5,2.9-3.4,2.9-5.6l0-10.6l10.3,0l0,10.6c0,4.8-2.3,8.7-6.9,11.9
			c4.6,3.2,6.9,7.2,6.9,11.9L130.8,62.4z"></path>
		<path fill="#43e700" class="word-ekrit" d="M173.3,33.2c-0.1,4.5-2.1,8.2-6,11.3c-4,3.1-8.8,4.6-14.5,4.6h-6.4l0,13.3l-10.3,0l0-45.2l16.9,0.1
			c5.7,0,10.6,1.6,14.5,4.7C171.4,25,173.3,28.7,173.3,33.2z M163,33.1c0.1-2.2-0.9-4.1-2.9-5.6c-2-1.5-4.4-2.3-7.2-2.3l-6.6,0
			l0,15.7l6.5,0c2.8,0,5.2-0.7,7.2-2.3C161.9,37.2,162.9,35.3,163,33.1z"></path>
		<path fill="#43e700" class="word-ekrit" d="M262.4,26l-15.6,0l0,36.4l-10.3,0l0-36.4l-15.5,0l0-8.8l41.4,0L262.4,26z"></path>
		<path fill="#43e700" class="word-ekrit" d="M177.1,62.4l10.4,0l19-29.1l0,29.1l10.4,0l0-45.2l-10.4,0l-19,29.9l0-29.5l-10.3,0L177.1,62.4z"></path>
		<g>
			<path fill-rule="evenodd" class="word-ekrit" clip-rule="evenodd" fill="#43e700" d="M52,23.9c-3.3-2.5-7.2-3.7-11.4-3.7c-2.1,0-4.2,0.3-6.1,1
				c-1.9,0.7-3.8,1.6-5.4,2.7c-1.6,1.2-3.1,2.6-4.3,4.2c-0.9,1.2-1.6,2.4-2.3,3.8h36C57,28.7,54.8,26,52,23.9z"></path>
			<path fill-rule="evenodd" class="word-ekrit" clip-rule="evenodd" fill="#43e700" d="M40.5,0.3C18.5,0.3,0.7,18.1,0.7,40s17.8,39.8,39.8,39.8
				S80.3,62,80.3,40S62.5,0.3,40.5,0.3z M22.3,47.9c1.1,2.4,2.5,4.5,4.3,6.2c1.8,1.8,3.9,3.2,6.3,4.2c2.4,1,4.9,1.6,7.6,1.6
				c3.7,0,7.1-0.9,10.1-2.8c3-1.8,5.4-4.3,7.1-7.4l0.3-0.6h10.6L68,50.8c-1.5,3.8-3.8,7.2-6.7,10.1c-2.8,2.7-6,4.9-9.5,6.4
				c-3.6,1.5-7.3,2.3-11.3,2.3c-3.9,0-7.8-0.8-11.3-2.3c-3.6-1.5-6.8-3.6-9.5-6.4c-2.8-2.8-5-6-6.4-9.6C11.8,47.8,11,44,11,40.1
				c0-3.9,0.7-7.7,2.2-11.3c1.5-3.6,3.6-6.8,6.4-9.5c2.7-2.8,5.9-5,9.5-6.5c3.6-1.5,7.4-2.3,11.3-2.3c3.9,0,7.7,0.8,11.3,2.3
				c3.5,1.5,6.8,3.7,9.5,6.5c2.8,2.7,5,6,6.4,9.6c1.5,3.6,2.2,7.5,2.2,11.4v1.2H20.7C20.9,43.8,21.4,45.9,22.3,47.9z"></path>
		</g>
	</g>
	</svg>			
</a>
</div>
<div id="loader">
	<div class="loader-block">
		<div class="loader">
		</div>
		<svg width="70" class="loader-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 205.5 205.5" enable-background="new 0 0 205.5 205.5" xml:space="preserve">
		<circle fill-rule="evenodd" clip-rule="evenodd" fill="#3FDA00" cx="101.4" cy="103.6" r="98.8"/>
		<path fill="#FFFFFF" d="M174.5,104.3c0-12.1-2.9-24.1-8.5-34.8c-5.2-10-13-18.6-22.2-25.3C124.3,30,98.3,26.4,75.6,35
			C54.2,43.1,37.2,61.6,31,83.5c-6.5,23.1-1.3,48.5,14,66.9c15.3,18.3,38.8,28.3,62.6,26.2c23.2-2,44.7-15.6,56.7-35.5
			c2.9-4.7,5-9.8,7-14.9l-26.2-0.2c-2.3,4.1-4.9,8.2-8.2,11.6c-3.7,3.9-8.1,7.2-12.8,9.7c-10.2,5.4-22.5,6.9-33.8,4.3
			c-20.5-4.7-36.8-23.3-38.1-44.4h122.2C174.5,107.3,174.5,104.6,174.5,104.3z M101.4,54.3c9.5,0,18.9,2.7,26.8,8
			c7.7,5.2,13.9,12.5,17.8,20.9H56.5C64.6,65.9,82.2,54.3,101.4,54.3z"/>
		</svg>
	</div>
</div>